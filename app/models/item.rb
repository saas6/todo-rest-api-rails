class Item < ApplicationRecord
  #model associated
  belongs_to :todo

  #validation
  validates_presence_of :name
end