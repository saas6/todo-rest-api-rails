# spec/factories/users.rb
FactoryBot.define do
  factory :user do
    name { Faker::Name.name }
    email 'yoyo@unipi.com'
    password 'yoyo'
  end
end